package com.example.ricardo.flaggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Finish extends AppCompatActivity {
    private TextView FinalScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        Intent intent = getIntent();
        int finalScore = intent.getIntExtra("FinalScore", 0);

        FinalScore = (TextView) findViewById(R.id.finalScore);
        FinalScore.setText(FinalScore.getText() + String.valueOf(finalScore));
    }

    public void PlayAgain(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
