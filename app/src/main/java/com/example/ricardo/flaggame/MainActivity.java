package com.example.ricardo.flaggame;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.Serializable;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    String url = "http://country.io/names.json";
    ProgressBar progressBar;
    HashMap<String, String> countryCode = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.substring(1, response.length() - 1);
                String[] formatChars = response.split("\\:|,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

                if(formatChars != null){
                    for(int i = 0; i < formatChars.length; i = i + 2){
                        if(Character.isWhitespace(formatChars[i].charAt(0))){
                            formatChars[i] = formatChars[i].substring(1);
                        }
                        if(Character.isWhitespace(formatChars[i + 1].charAt(0))){
                            formatChars[i + 1] = formatChars[i + 1].substring(1);
                        }

                        countryCode.put(formatChars[i], formatChars[i + 1]);
                    }
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    public void StartGame(View view){
        Intent intent = new Intent(this, Game.class);
        intent.putExtra("CountryCode", (Serializable) countryCode);
        startActivity(intent);
    }
}
