package com.example.ricardo.flaggame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Game extends AppCompatActivity {
    private ImageView mImg;
    private Button mOptions[] = new Button[4];
    private String Answer;
    private TextView ScoreLine;
    private int Score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mImg = (ImageView) findViewById(R.id.imageView2);
        ScoreLine = (TextView) findViewById(R.id.scoreLine);

        Intent intent = getIntent();
        HashMap<String, String> countryCode = (HashMap<String, String>)intent.getSerializableExtra("CountryCode");
        Score = intent.getIntExtra("Score", 0);

        ScoreLine.setText(ScoreLine.getText() + String.valueOf(Score));

        Log.i("UntilEnd", Integer.toString(countryCode.size()));

        if(countryCode.size() <= 240){
            Intent finish = new Intent(this, Finish.class);
            finish.putExtra("FinalScore", Score);
            startActivity(finish);
        }

        setAll(countryCode);
    }

    private void setAll(HashMap<String, String> countryCode){
        mOptions[0] = (Button) findViewById(R.id.op1);
        mOptions[1] = (Button) findViewById(R.id.op2);
        mOptions[2] = (Button) findViewById(R.id.op3);
        mOptions[3] = (Button) findViewById(R.id.op4);

        //Get Random
        Object[] crunchKeys = countryCode.keySet().toArray();
        Object answer = crunchKeys[new Random().nextInt(crunchKeys.length)];
        Answer = countryCode.get(answer).replace("\"","");

        //Shuffle buttons
        Set<String> removeList = new HashSet<>();
        List<String> list = new ArrayList<>();
        list.add(countryCode.get(answer).replace("\"",""));
        removeList.add(answer.toString());

        for(int i = 0; i < mOptions.length - 1; i++){
            String randKeyGet = crunchKeys[new Random().nextInt(crunchKeys.length)].toString();
            list.add(countryCode.get(randKeyGet).replace("\"", ""));
            removeList.add(randKeyGet);
        }

        countryCode.keySet().removeAll(removeList);

        Collections.shuffle(list);
        for(int i = 0; i < mOptions.length; i++){
            mOptions[i].setText(list.get(i));
        }

        //Request country flag
        Requester(answer);
    }

    private void Requester(Object answer){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.countryflags.io/" + answer.toString().substring(1, answer.toString().length() - 1) + "/flat/64.png";

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true);

        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                dialog.dismiss();
                mImg.setImageBitmap(response);
                mImg.getLayoutParams().height = 600;
                mImg.getLayoutParams().width = 500;
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(Game.this, error.toString(), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });

        queue.add(imageRequest);
    }

    public void onClick(View view){
        Button button = (Button)view;
        if(button.getText().toString().equals(Answer)){
            Toast.makeText(Game.this, "Right Answer", Toast.LENGTH_SHORT).show();
            Score += 10;
            Intent intent = getIntent();
            intent.putExtra("Score",Score);
            startActivity(intent);
            finish();
        }else{
            Score -= 5;
            Toast.makeText(Game.this, "Wrong Answer", Toast.LENGTH_SHORT).show();
        }
    }
}
